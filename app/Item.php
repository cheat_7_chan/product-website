<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "items";

    public function cate()
    {
    	return $this->belongsTo('App\Item_Cat','category_id');
    }
    public function item_cat_page()
    {
        return $this->belongsToMany('App\Category','item_categorys_page','items_category_id','items_id');
    }
    public function image_item()
    {
        return $this->hasMany('App\Item_image','item_id');
    }
}
