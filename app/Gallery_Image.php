<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_Image extends Model
{
    protected $table = "galleries_image";
}
