<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_Cat extends Model
{
    protected $table = "item_categorys";

    public function items()
    {
    	return $this->hasMany('App\Item','category_id');
    }
    public function item_page()
    {
        return $this->belongsToMany('App\Item_cat_page','items_id','items_category_id');
    }
}
