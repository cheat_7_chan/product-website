<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item_Cat;
use Auth;
use DB;
use App\Item_cat_page;
use App\Category;
use App\Item;

class ProductController extends Controller
{

    public function index_product_category()
    {
    	$item_category = Item_Cat::where('category_type','=','product')->get();
    	return view('admin.product.category.category',compact('item_category','cat_item'));
    }
    public function get_create_product_category()
    {
    	$item_category = Item_Cat::where('category_type','=','product')->get();
    	return view('admin.product.category.category',compact('item_category','cat_item'));
    }
    public function post_create_product_category(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required|unique:category'
            ]);
            
        if($request->link != Null){
            $link = str_slug($request->link ,'-');
        }else{
            $link = str_slug($request->name,'-');
        }

    	$item_category = [
            'name' => $request->name,
            'link' => $link,
    		'description' => $request->description,
            'category_type' => 'product',
            'parent_id' => $request->parent_id,
            'status' => $request->status,
            'user_id' => Auth::user()->id,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date,
            'created_at' => date('Y-m-d'),
    		];

            Item_Cat::insert($item_category);

        return redirect()->to('products-category/create')->with('success','Successful Create');
    }
    public function get_edit_product_category($id)
    {
        $post = Item_Cat::find($id);
        $item_category = Item_Cat::where('category_type','=','product')->get();
        return view('admin.product.category.category',compact('item_category','post'));
    }

    public function post_edit_product_category(Request $request,$id)
    {
       $this->validate($request,[
            'name' => 'required'
            ]);
        if($request->link != Null){
            $link = str_slug($request->link ,'-');
        }else{
            $link = str_slug($request->name,'-');
        }
        $item_category = Item_Cat::find($id);
        $item_category = [
            'name' => $request->name,
            'link' => $link,
            'description' => $request->description,
            'category_type' => 'product',
            'parent_id' => $request->parent_id,
            'status' => $request->status,
            'user_id' => Auth::user()->id,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date,
            'updated_at' => date('Y-m-d'),
            ];

        $cat_item = Item_Cat::where('id','=',$id)->update($item_category);

        return redirect()->to('products-category')->with('success','Updated Successful');
    }

    public function get_delete_product_category($id)
    {
        $cat_item = Item_Cat::find($id);
        $cat_item->delete();
        return redirect('products-category')->with('success', 'Deleted Successful');
    }
//Brand

    public function index_product_brand()
    {
    	$item_category = Item_Cat::where('category_type','=','product-brand')->get();
    	return view('admin.product.category.brand',compact('item_category'));
    }
    public function get_create_product_brand()
    {
    	$item_category = Item_Cat::where('category_type','=','product-brand')->get();
    	return view('admin.product.category.brand',compact('item_category'));
    }
    public function post_create_product_brand(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required'
    		]);
    	$item_category = [
    		'name' => $request->name,
    		'description' => $request->description,
            'category_type' => 'product-brand',
            'parent_id' => $request->parent_id,
            'status' => $request->status,
            'user_id' => Auth::user()->id,
            'publish_date' => $request->publish_date,
            'unpublish_date' => $request->unpublish_date,
            'created_at' => date('Y-m-d'),
    		];

            Item_Cat::insert($item_category);

        return redirect()->to('products-brand/create')->with('success','Successful Create');
    }
    public function get_edit_product_brand($id)
    {
        $cat_item = Item_Cat::find($id);
        $item_category =Item_Cat::where('category_type','=','product-brand')->get();
        return view('admin.product.category.brand',compact('item_category','cat_item'));
    }

    public function post_edit_product_brand(Request $request,$id)
    {
       $this->validate($request,[
            'name' => 'required'
            ]);

        $item_category = [
            'name' => $request->name,
            'description' => $request->description,
            'category_type' => 'product-brand',
            'parent_id' => $request->parent_id,
            'status' => $request->status,
            'user_id' => Auth::user()->id,
            'publish_date' => $request->publish_date,
            'u
             npublish_date' => $request->unpublish_date,
            'updated_at' => date('Y-m-d'),
            ];

        $cat_item = Item_Cat::where('id','=',$id)->update($item_category);

        return redirect()->to('products-category')->with('success','Updated Successful');
    }

    public function get_delete_product_brand($id)
    {
        $cat_item = Item_Cat::find($id);
        $cat_item->delete();
        return redirect('products-brand')->with('success', 'Deleted Successful');
    }

// Product 

        public function index_product()
        {   
            $category = Category::where('category_type','=','category')->with('item_post')->get();
            // dd($cat);
            $item_category = Item_Cat::where('category_type','=','product')->get();
            $item = Item::where('post_type','=','product')->get();
            return view('admin.product.index_product',compact('category','item','item_category'));
        }
        public function get_create_product()
        {
            $item_category = Item_Cat::where('category_type','=','product')->get();
            $category = Category::where('category_type','=','category')->with('item_post')->get();
            $item = Item::all();
            return view('admin.product.create_product',compact('item','item_category','category'));
        }
        public function post_create_product(Request $request)
        {

            $this->validate($request,[
                'title' => 'required'
            ]);

        if($request->link != Null){
            $link = str_slug($request->link ,'-');
        }else{
            $link = str_slug($request->title,'-');
        }
            if($request->hasFile('image')){
                $images = $request->file('image');
                $destinationPath = "images/";
                $fileName = $images->getClientOriginalName();
                $fileupload = $images->move($destinationPath,$fileName);
            }else{
                $fileName ="";
            }

            $postcout = Item::get();
            if(count($postcout) > 0){
            $pos_last_id = collect(Item::orderBy('id','desc')->get())->last();
            $id_post = $pos_last_id->id + 1;
            }else{
            $id_post = 1;
            }
            $post = [
                'id' => $id_post,
                'title' => $request->title,
                'link' => $link,
                'category_id'  => $request->category_id,
                'link_download' =>$request->link_download,
                'language' =>$request->language,
                'description' => $request->description,
                'items_category_id' => $request->items_category_id,
                'cost_price' => $request->cost_price,
                'sell_price' => $request->sell_price,
                'discount' => $request->discount,
                'recode_level' => $request->recode_level,
                'status' => $request->status,
                'publish_date' => $request->publish_date,
                'post_type' => 'product',
                'image' => $fileName,
                'user_id' => Auth::user()->id,
                'unpublish_date' => $request->unpublish_date,
                'created_at' => date('Y-m-d'),
            ];

            $post_id = Item::insertGetId($post);
            // dd($request->dd);
            if ($post_id != 0) {
                if ($request->has('items_category_id')) {
                    
                        $post_category = [
                            'items_id' => $post_id,
                            'items_category_id' => $request->items_category_id,
                        ];

                        DB::table('item_categorys_page')->insert($post_category);

                }
                   
                    
            if($request->hasFile('image_multi')){
                foreach ($request->file('image_multi') as $ke => $value) {
                        $images = $request->file('image_multi')[$ke];
                        $destinationPath = "images/";
                        $fileName = $images->getClientOriginalName();
                        $fileupload = $images->move($destinationPath,$fileName);
                        $post_image = [
                            'item_id' => $post_id,
                            'images' => $request->get('images/',$fileName)
                        ];

                        DB::table('item_images')->insert($post_image);
                }
            }

            }



            return redirect()->to('products-product/create')->with('success','Created Successful');
        }

        public function get_edit_product($id)
        {
            
            $item = Item::find($id);
            $category = Category::where('category_type','=','category')->with('item_post')->get();
            $cate_page = Item_cat_page::get();
            $item_category = Item_Cat::where('category_type','=','product')->get();
            $post = Item::where('post_type','=','product')->where('id','=',$id)->first();
            return view('admin.product.edit_product',compact('item','cate_page','item_category','post','posts','category'));
        }

        public function post_edit_product(Request $request,$id)
        {

            $this->validate($request,[
                'title' => 'required'
            ]);
            if($request->link != Null){
            $link = str_slug($request->link ,'-');
            }else{
            $link = str_slug($request->title,'-');
            }
            $post = Item::find($id);

            if($request->hasFile('image')){
                $images = $request->file('image');
                $destinationPath = "images/";
                $fileName = $images->getClientOriginalName();
                $fileupload = $images->move($destinationPath,$fileName);
            }else{
                $fileName =$post->image;
            }

            $post = [ 
                'title' => $request->title,
                'link' => $link,
                'category_id'  => $request->category_id,
                'link_download' =>$request->link_download,
                'language' =>$request->language,
                'description' => $request->description,
                'items_category_id' => $request->items_category_id,
                'cost_price' => $request->cost_price,
                'sell_price' => $request->sell_price,
                'discount' => $request->discount,
                'recode_level' => $request->recode_level,
                'status' => $request->status,
                'publish_date' => $request->publish_date,
                'post_type' => 'product',
                'image' => $fileName,
                'user_id' => Auth::user()->id,
                'unpublish_date' => $request->unpublish_date,
                'created_at' => date('Y-m-d'),
                    ];

            $post_id = Item::where('id','=',$id)->update($post);

            DB::table('item_categorys_page')->where('items_id','=',$id)->delete();
            if ($request->has('items_category_id')) {
                
                        $post_category = [
                        'items_id' => $id,
                        'items_category_id' => $request->items_category_id,
                        ];

                        DB::table('item_categorys_page')->insert($post_category);
                }
            
            if($request->hasFile('image_multi')){
                DB::table('item_images')->where('item_id','=',$id)->delete();
                foreach ($request->file('image_multi') as $ke => $value) {
                        $images = $request->file('image_multi')[$ke];
                        $destinationPath = "images/";
                        $fileName = $images->getClientOriginalName();
                        $fileupload = $images->move($destinationPath,$fileName);
                        $post_image = [
                            'item_id' => $id,
                            'images' => $request->get('images/',$fileName)
                        ];

                        DB::table('item_images')->insert($post_image);
                }
            }

            return redirect()->to('products-product')->with('success','Updated Successful');
        }
        public function get_delete_product($id)
        {
            DB::table('item_categorys_page')->where('items_id','=',$id)->delete();
            $post = Item::find($id);
            DB::table('item_images')->where('item_id','=',$id)->delete();
            $post->delete();
            return redirect()->to('products-product')->with('success','Deleted Successful');
        }
    
}
