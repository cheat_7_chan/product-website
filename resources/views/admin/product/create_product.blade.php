@extends('admin.layout.master')
@section('content')
	<div class="row">
        <div class="col-md-12">

			<h2><strong>Products Courses </strong></h2>
		       @include('errors.message_error')
		</div>
	</div>
	<hr/>
	<div class="row">
    <form action="{{ url('products-product/create') }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
		<div class="col-md-6">
			
			   <div class="col-md-11">
              <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" class="form-control">
              </div>
              <div class="form-group">
                <label>Link</label>
                <input type="text" name="link" class="form-control">
              </div>
              <div class="form-group">
                  <label>Language</label>
                  <?php $lang = App\Language::get(); ?>
                  <select class="form-control" name="language">
                      @if(count($lang) > 0)
                          @foreach($lang as $la)
                            <option value="{{ $la->id }}"> {{ $la->name }} </option>
                          @endforeach
                      @endif
                  </select>
              </div>
               <div class="form-group">
                <label>Categories</label><br>
                  <select class="form-control" name="category_id">
                    <option value="0">Select Category</option>
                    @if(count($item_category)>0)
                      @foreach($item_category as $c)
                          @if($c->category_type == 'product')
                            <option value="{{$c->id}}">{{$c->name}}</option>
                          @endif
                      @endforeach
                    @endif
                  </select>
              </div>
               <div class="form-group">
                <label>Brand</label><br>
                  <select class="form-control" name="brand_id[]">
                    <option value="0">Select Brand</option>
                    @if(count($item_category)>0)
                      @foreach($item_category as $c)
                          @if($c->category_type == 'product-brand')
                            <option value="{{$c->id}}">{{$c->name}}</option>
                          @endif
                      @endforeach
                    @endif
                  </select>
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="ckeditor" id="editor" placeholder="Enter Description"></textarea>
              </div>
              <div class="form-group">
                <label>Link Download</label>
                <input type="text" name="link_download" class="form-control">
              </div>
              <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" name="status">
                      <option value="1">Active</option>
                      <option value="0">Not Active</option>
                  </select>
              </div>
              <div class="form-group">
                <label>Publish Date</label>
                <div class='input-group date' id='datetimepicker1'>
                  <input type='text' name="publish_date" class="form-control" />
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>Unpublish Date</label>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' name="unpublish_date" class="form-control" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="form-group">
                <label>Choose Image Feature</label>
                <input type="file" name="image" class="form-control">
              </div>
							<div class="form-group">
                <label>Choose Image Multi Upload</label>
                <input type="file" name="image_multi[]" class="form-control" multiple>
              </div>
              <button type="submit" class="btn btn-primary">Submit </button>
           </div>
			
		</div>
    <!-- add categories    -->
    
    <div class="col-md-6">
    <div class="form-group">
                <label>Discount</label>
                <input type="text" name="discount" class="form-control">
        </div>
        <div class="form-group">
                <label>Cost Price</label>
                <input type="text" name="cost_price" class="form-control">
        </div>
        <div class="form-group">
                <label>Sell Price</label>
                <input type="text" name="sell_price" class="form-control">
        </div>
        <div class="form-group">
            <label>Button Open or Close</label> 
            <input type="checkbox"  name="recode_level"  class="editor" value="1" style=" margin: 1px 13px;">
          </div>
        <br/>
						<table class="table table-striped table-bordered">
							<thead>
									<tr>
										<th>Select Category</th>
										<th><a href="#" id="add_on_rows" class="btn btn-success">Add</a></th>
									</tr>
							</thead>
							<tbody id="body_add">
						
								<tr id="add_row">
									<td>
                  <?php $category = App\Category::where('category_products','=','1')->get(); ?>
										<select class="form-control" name="items_category_id">
                    <option value="0">Select Catgory</option>
											@if(count($category)>0)
												@foreach($category as $cat)
														<option value="{{ $cat->id }}">{{$cat->name}}</option>
												@endforeach
											@endif
										</select>
									</td>
									<td><a href="#" class="btn btn-danger remove_rows" >delete</a></td>
								</tr>

							</body>
						</table>
					</div>
  	</div>
    </form>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$('#add_on_rows').click(function(event){
				 event.preventDefault();
						var rows = '<tr id="add_row">' +
											 '<td>' +
											 '<select class="form-control" name="items_category_id">' +
	 											'@if(count($category)>0)' +
	 											'<option value="">Select Catgory</option>' +
	 												'@foreach($category as $key => $cat)' +
	 														'<option value="{{ $cat->id }}">{{$cat->name}}</option>' +
	 												'@endforeach' +
	 											'@endif' +
	 										'</select>'  +
	 									'</td>'  +
											 '<td><a href="#" class="btn btn-danger remove_rows" >delete</a></td>'
											 '</tr>';
					 $('#body_add').append(rows)
			 });
			 $('#body_add').on('click','.remove_rows',function(event){
						 event.preventDefault();
						$(this).parent().parent().remove();
				});
});
</script>
@endsection
