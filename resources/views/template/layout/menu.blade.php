<?php
    if(Request::segment(1) == 'kh'){
        $language = "ភាសា";
    }elseif(Request::segment(1) == 'en'){
        $language = "Language";
    }elseif(Request::segment(1) == 'cn'){
        $language = "语言";
    }else{
        $language = "Language";
    }
?>
<?php $lang = App\Language::where('status','=',1)->get(); ?>
<?php
    function display_menu($parent_id = 0){
        if(Request::segment(1) == 'kh'){
            $local= 2;
        }elseif(Request::segment(1) == 'cn'){
            $local= 3;    
        }elseif(Request::segment(1) == 'en'){
            $local= 1;
        }else{
            $local= 1;
        }
        $menu = App\Menu::where('parent_id','=',$parent_id)->where('language',$local)->where('status',1)->orderBy('ordering')->get();
        // echo "<ul>";
          foreach($menu as $key => $val){
            //$url_current = url()->current();

            if(Request::segment(1) == 'en'){
                if($val->link != "#"){
                    $url = url('en/'.$val->link);
                }else{
                    $url = url($val->link);  
                }
                $url_current = Request::segment(2);
            }elseif(Request::segment(1) == 'kh'){
                if($val->link != "#"){
                    $url = url('kh/'.$val->link);
                }else{
                    $url = url($val->link);  
                }
                 $url_current = Request::segment(2);
            }elseif(Request::segment(1) == 'cn'){
                if($val->link != "#"){
                    $url = url('cn/'.$val->link);
                }else{
                    $url = url($val->link);  
                }
                $url_current = Request::segment(2);
            }else{ 
                $url = url($val->link);
                $url_current = Request::segment(1);
            }
            if($url_current == $val->link){
                $active = "class=active";
            }else{ 
                
                $active = "";
            }
            
             echo '<li  class="nav-item" '.$active.'><a class="nav-link" href="'.$url.'" >'.$val->name.'</a>';
            display_menu($val->id);
            echo"</li>";
        }
        // echo "</ul>";
    } 
?>
<style type="text/css">
    ul.submenu {
        display: block;
        justify-content: normal;
    }
    .contact-icon {
        display: flex;
    }
    .header-bottom {
        box-shadow: 0px 1px 1px rgba(0,0,0,.1);
    }
    @media(max-width: 430px) {
        .contact-info-lang {
            display: none;
        }
    }
    @media (min-width: 992px) and (max-width: 1440px) {
        .contact-info-lang {
            display: none;
        }
    }

    .sticky {
    position: fixed;
    z-index: 1;
    top: 0;
    width: 100%;
    }

    

</style>
<?php $menu_na = App\Menu::where('name','home')->first();
?>
@if(Request::segment(1) == 'home' || Request::segment(2) == 'home')
<nav class="navbar navbar-expand-lg navbar-light osahan-menu-2 pad-none-mobile" id="myHeader">
@else
<nav class="navbar navbar-expand-lg navbar-light osahan-menu-2 pad-none-mobile mb-0" id="myHeader" style="border-bottom: 2px solid #eaeaea;">
@endif
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0 margin-auto">
                <li class="nav-item"  >
                    <a class="nav-link shop" href="{{url('home')}}">
                        <span class="mdi mdi-store"></span>{{$menu_na->name}}
                    </a>
                </li>
                <?php display_menu(); ?>
                <li class="nav-item dropdown">
                     <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Pages
                     </a>
                     <div class="dropdown-menu">
                         <div class="row">
                            <div class="col-md-3">
                            <a class="dropdown-item" href="https://askbootstrap.com/preview/osahan-grocery-v1-4/shop.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Shop Grid</a>
                            <a class="dropdown-item" href="https://askbootstrap.com/preview/osahan-grocery-v1-4/single.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Single Product</a>
                            <a class="dropdown-item" href="https://askbootstrap.com/preview/osahan-grocery-v1-4/cart.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Shopping Cart</a>
                            <a class="dropdown-item" href="https://askbootstrap.com/preview/osahan-grocery-v1-4/checkout.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Checkout</a> 
                            </div>
                            <div class="col-md-9 images">
                                <img src="{{url('images/ad/1.jpg')}}" alt="">
                            </div>
                         </div>
                       
                     </div>
                  </li>
            </ul>
        </div>
    </div>
</nav>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>