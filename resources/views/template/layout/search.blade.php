
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
      .list{
         cursor: pointer;
         padding:6px;
         -webkit-user-select: none;
         list-style:none;
      }
      .span{
         display:block;
         background:white;
         box-shadow: 0 4px 4px #dcdcdc!important;
      }
      .select_cat{
         border: 2px solid #e96125 !important;
         border-right: 0px !important;
         border-top-left-radius: 4px;
         border-bottom-left-radius: 4px;
      }
      .search_pro{
         border: 2px solid #e96125 !important;
         border-left: 0px !important;
      }
  </style>
<nav class="navbar navbar-light navbar-expand-lg bg-dark bg-faded osahan-menu">
   <div class="container-fluid">
   <?php $set = App\Setting::get(); ?>
      <a class="navbar-brand" href="{{url('home')}}"> 
      @foreach($set as $s)
      <img src="{{url('images/'.$s->logo_image)}}" alt="logo"> 
      @endforeach
      </a>
   
      <button class="navbar-toggler navbar-toggler-white" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse" id="navbarNavDropdown">
         <div class="navbar-nav mr-auto mt-2 mt-lg-0 margin-auto top-categories-search-main">
            <div class="top-categories-search">
               <div class="input-group" style="flex-wrap: nowrap;">
                  <span class="input-group-btn categories-dropdown " >
                     <div class="select_cat">
                     <select class="form-control-select " id="category_id">
                        <?php $cat = App\Item_Cat::where('category_type','=','product')->get(); ?>
                        @if(count($cat)>0)
                              <option selected="selected"  value="0">All Category</option>
                           @foreach($cat as $cate)
                              <option value="{{$cate->id }}">{{$cate->name}}</option>
                              @foreach($cate->items as $it)
                              <?php
                                 if(Request::segment(1) == 'kh') {
                                    $url = url('kh/'.$it->link.'/'.$it->id.'/detail');
                                 }elseif(Request::segment(1) == 'en') {
                                    $url = url('en/'.$it->link.'/'.$it->id.'/detail');
                                 }elseif(Request::segment(1) == 'cn') {
                                    $url = url('cn/'.$it->link.'/'.$it->id.'/detail');
                                 }else {
                                    $url = url($it->link.'/'.$it->id.'/detail');
                                 }
                              ?>
                           @endforeach
                           @endforeach
                        @endif
                     </select>
                     </div>
                  </span>
                  <div class="form-group " style="width:60%;margin-bottom:0rem;" >
                  
                     <input class="form-control search_pro" style="border-radius:0px; " autocapitalize="none" spellcheck="false" autocomplete="off" autocorrect="off" tabindex="0" autocapitalize="none"  role="textbox" name="title" id="produts_name" placeholder="Search.." type="search">
                     <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                     <span class="span" id="productslist">
                     </span>
                  </div>
                  <span class="input-group-btn">
                  <button id="search" class="btn btn-secondary" type="submit" onClick="btn_search()"><i class="mdi mdi-file-find" ></i> Search</button>
                  </span>
               </div>
               {{ csrf_field() }}
            </div>
           
         </div>
         
      </div>
   </div>
</nav>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
$(document).ready(function(){
  
   $('#produts_name').keyup(function(){ 
      if($("#produts_name").val()==''){
         $('#productslist').css("display","none");
      }else{
         
         var category_id = $('#category_id').val();
         var query = $(this).val();
         var _token = $('input[name="_token"]').val();
         $.ajax({
            url:"{{ route('search/fetch') }}",
            method:"POST",
            data:{query:query, category_id:category_id, _token:_token},
            success:function(data){
               var ul = "";
               
               ul +='<div class="list-group dropdown-menu" style="display:block; position:relative; padding:0;">';
               $.each(data,function(u,n){
                  var link = '{{url('')}}'+'/'+n.link+'/'+n.id+'/detail';
                  ul +='<a  href='+link+' class="list list-group-item list-group-item-action" value="stores_item" style="border: 1px solid #fff;"; id='+n.id+'>'+n.title+'</a>';
               }); 
               ul +='</div>'
               console.log(data);
               $('#productslist').fadeIn();  
               $('#productslist').html(ul);
            }
         });
      }
      
   }); 
   
   $(document).on('click', 'li', function(){
         $('#produts_name').val($(this).text());  
         $('#productslist').fadeOut(); 
   });  

   

  
});


</script>
<script>
   function btn_search()
   {
      // if("{{Request()->segment(1)}}" == 'kh'){
      //    var locale = 'kh';
      // }else{
      //    var locale = 'en';
      // }
      var search = document.getElementById("produts_name").value;
      // console.log(search);
      location.href = "{{ url('') }}"+"/search_product?title="+search;
      // location.href="{{url('search_poduct')}}";
   }
</script>