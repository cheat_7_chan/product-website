<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      
      <?php $set = App\Setting::get(); ?>
      @foreach($set as $s)
      <title>{{$s->website_name}}</title>
      @endforeach
      <!-- Favicon Icon -->
      <?php $set = App\Setting::get(); ?>
      @foreach($set as $s)
      <link rel="icon" type="images/png" href="{{url('images/'.$s->favicon_image)}}">
      @endforeach
      <!-- Bootstrap core CSS -->
      <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
      <!-- Material Design Icons -->
      <link href="{{url('vendor/icons/css/materialdesignicons.min.css')}}" media="all" rel="stylesheet" type="text/css" />
      <!-- Select2 CSS -->
      <link href="{{url('vendor/select2/css/select2-bootstrap.css')}}" />
      <link href="{{url('vendor/select2/css/select2.min.css')}}" rel="stylesheet" />
      <!-- Custom styles for this template -->
      <link href="{{url('css/osahan.min.css')}}" rel="stylesheet">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="{{url('vendor/owl-carousel/owl.carousel.css')}}">
      <link rel="stylesheet" href="{{url('vendor/owl-carousel/owl.theme.css')}}">
      <!-- auto complete -->
      <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'>
      
      
   </head>
   <body>

        
      
        <!-- search-area -->
        @include('template.layout.search')
        <!-- search-area -->
        @include('template.layout.menu')

        @if(Request::segment(1) == 'home' || Request::segment(2) == 'home')
            <!-- slider area start -->
            @include('template.layout.slide')
            <!-- slider area end -->
        @elseif(Request::segment(1) == 'about-us' || Request::segment(2) == 'about-us')
            <!-- breadcumb-area start -->
            @include('template.layout.breadcumb')
            <!-- breadcumb-area end -->
        @elseif(Request::segment(1) == 'contact' || Request::segment(2) == 'contact')
            <!-- breadcumb-area start -->
            @include('template.layout.breadcumb')
            <!-- breadcumb-area end -->
        @else
            @include('template.layout.head')
        @endif

        @yield('content')

        <!-- footer -->
        @include('template.layout.footer')
        <!-- endfooter -->
      
      <!-- End Copyright -->
      
      <!-- Bootstrap core JavaScript -->
      <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
      <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
      <!-- select2 Js -->
      <script src="{{url('vendor/select2/js/select2.min.js')}}"></script>
      <!-- Owl Carousel -->
      <script src="{{url('vendor/owl-carousel/owl.carousel.js')}}"></script>
      <!-- Custom -->
      <script src="{{url('js/custom.min.js')}}"></script>
	  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120909275-1"></script>
	  <script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-120909275-1');
	  </script>

    
   </body>
</html>

