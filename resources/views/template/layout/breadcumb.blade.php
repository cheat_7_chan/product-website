<?php $menu_im = App\Menu::where('link',$link)->where('language',$local)->first(); ?>
<?php $menu_na = App\Menu::where('language',$local)->where('link',$link)->first();
?>
@if(count($menu_im) > 0)
<section class="section-padding bg-orange inner-header">
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
		<?php $menu_home = App\Menu::where('link','=','home')->where('language',$local)->first(); ?>
			<h1 class="mt-0 mb-3 text-white">{{ $menu_na->name }}</h1>
			<div class="breadcrumbs">
				<p class="mb-0 text-white"><a class="text-white" href="{{ url('/'.$menu_home->link) }}">{{ $menu_home->name }}</a>  /  <span class="text-success">{{ $menu_na->name }}</span></p>
			</div>
		</div>
	</div>
</div>
</section>
@endif