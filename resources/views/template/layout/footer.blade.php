<?php   
    if(Request::segment(1) == 'kh'){
        $local= 2;
    }elseif(Request::segment(1) == 'cn'){
        $local= 3;    
    }elseif(Request::segment(1) == 'en'){
        $local= 1;
    }else{
        $local= 1;
    }
    $setting= App\Setting::where('language',$local)->get();
    $widget = App\Widget::where('language',$local)->get();
?>
  <!-- Footer -->

  <section class="section-padding footer bg-white border-top">
         <div class="container">
            <div class="row">
               @foreach($widget as $wid)
                    @foreach($setting as $settings)
                        @if($wid->position == 'footer_1')
                            @if($wid->page_side)
                                <?php  $tem = $wid->page_side; ?>
                            @else
                                <?php   $tem = "non"; ?>
                            @endif
                                @include('template.'.$tem)
                        @endif
                    
                        @if($wid->position == 'footer_2')
                            @if($wid->page_side)
                                <?php  $tem = $wid->page_side; ?>
                            @else
                                <?php   $tem = "non"; ?>
                            @endif
                                @include('template.'.$tem)
                        @endif

                        @if($wid->position == 'footer_3')
                            @if($wid->page_side)
                                <?php  $tem = $wid->page_side; ?>
                            @else
                                <?php   $tem = "non"; ?>
                            @endif
                                @include('template.'.$tem)
                        @endif

                        @if($wid->position == 'footer_4')
                            @if($wid->page_side)
                                <?php  $tem = $wid->page_side; ?>
                            @else
                                <?php   $tem = "non"; ?>
                            @endif
                                @include('template.'.$tem)
                        @endif
                        @if($wid->position == 'footer_5')
                            @if($wid->page_side)
                                <?php  $tem = $wid->page_side; ?>
                            @else
                                <?php   $tem = "non"; ?>
                            @endif
                                @include('template.'.$tem)
                        @endif
                    @endforeach
                @endforeach
            </div>
         </div>
      </section>
      <!-- End Footer -->
      <!-- Copyright -->
      <section class="pt-4 pb-4 footer-bottom">
         <div class="container">
            <div class="row no-gutters">
               <div class="col-lg-6 col-sm-6">
                  <p class="mt-1 mb-0">&copy; Copyright 2020 <strong class="text-dark">Osahan Grocery</strong>. All Rights Reserved<br>
                  @foreach($setting as $set)
				  <small class="mt-0 mb-0">Made with <i class="mdi mdi-heart text-danger"></i> by <a href="{{url('titb.biz')}}" target="_blank" class="text-primary">{{strip_tags($set->copyright)}}</a>
                  </small>
                  @endforeach
				  </p>
               </div>
               <div class="col-lg-6 col-sm-6 text-right">
                  <img alt="osahan logo" src="{{url('images/payment_methods.png')}}">
               </div>
            </div>
         </div>
      </section>