<?php $slides = App\Slide::where('language', $local)->get(); ?>


<section class="carousel-slider-main text-center border-top border-bottom bg-white">
	<div class="owl-carousel owl-carousel-slider">
		@foreach($slides as $slide)
		@foreach($slide->slide_image as $sl)
			<div class="item">
				<a href="shop.html">
					<img class="img-fluid" src="{{ url('Galleries/'.$sl->image) }}" alt="First slide">
				</a>
			</div>
		@endforeach
		@endforeach
	</div>
</section>