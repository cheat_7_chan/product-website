
<div class="col-lg-3 col-md-3">
    <h6 class="mb-4">Download App</h6>
    <div class="app">
        <a href="{{url('https://play.google.com/store/apps')}}" target="_blank"><img src="{{url('images/google.png')}}" alt=""></a>
        <a href="{{url('https://www.apple.com/ios/app-store/')}}" target="_blank"><img src="{{url('images/apple.png')}}" alt=""></a>
    </div>
    <h6 class="mb-3 mt-4">GET IN TOUCH</h6>
    <div class="footer-social">
        <?php $social = App\Social::get(); ?>
        @foreach($social as $so)
        <a class="btn-facebook" href="{{url($so->link)}}"><i class="{{$so->class}}"></i></a>
        @endforeach
        <!-- <a class="btn-twitter" href="my-profile.html#"><i class="mdi mdi-twitter"></i></a>
        <a class="btn-instagram" href="my-profile.html#"><i class="mdi mdi-instagram"></i></a>
        <a class="btn-whatsapp" href="my-profile.html#"><i class="mdi mdi-whatsapp"></i></a>
        <a class="btn-messenger" href="my-profile.html#"><i class="mdi mdi-facebook-messenger"></i></a>
        <a class="btn-google" href="my-profile.html#"><i class="mdi mdi-google"></i></a> -->
    </div>
</div>