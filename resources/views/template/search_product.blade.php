@extends('template.layout.master')

@section('content')
{{ csrf_field()}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
   .notice-danger {
    border-color: #d73814;
   }
   .notice-danger>strong {
      color: #d73814;
   }
   .notice {
    padding: 15px;
    background-color: #fafafa;
    border-left: 6px solid #d9534f;
    margin-bottom: 10px;
    -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
       -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
   }
   .notice-sm {
      padding: 10px;
      font-size: 80%;
   }
   .notice-lg {
      padding: 35px;
      font-size: large;
   }
</style>
@if(count($pos))
<section class="shop-list section-padding">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <a href="{{url('fruits-vegetables')}}"><img class="img-fluid mb-3" src="images/shop.jpg" alt="" width="100%"></a>
                  <div class="shop-head">
                     <a href="{{url('home')}}"><span class="mdi mdi-home"></span> Home</a> <span class="mdi mdi-chevron-right"></span> <a href="{{url('fruits-vegetables')}}">Fruits &amp; Vegetables</a> <span class="mdi mdi-chevron-right"></span> <a href="{{url('fruits-vegetables')}}">Fruits</a>
                     <div class="btn-group float-right mt-2  tab-content">
                        <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sort by Products &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                        <div class="dropdown-menu dropdown-menu-right"  id="filters">
                           <a class="dropdown-item sortByPrice" href="#">Price (Low to High)</a>
                           <a class="dropdown-item sortByPrice" href="#">Price (High to Low)</a>
                           <a class="dropdown-item sortByName" href="#" id="test">Name (A to Z)</a>
                        </div>
                     </div>
                     <h5 class="mb-3"></h5>
                  </div>
                  <div class="tab-pane active ">
                    <ul class="row no-gutters  results" id="list">
                    <?php 
                     
                     $post = $pos;
                     $len = $post->lastPage(); 
                     $txt = '';

                     for($i= 1 ; $i <=  $len; $i++){
                     if($post->currentPage() == $i){
                        $active='active';
                     }else{
                        $active='';
                     } 
                     $txt .= '<li class="page-item '.$active.'"><a class="page-link" href="'.request()->url().'?page='.$i.'">'.$i.'</a></li>';
                     }?>

                @foreach($post as $it)
                  @if($it->status ==1)
                        <?php
                            if(Request::segment(1) == 'kh') {
                                $url = url('kh/'.$it->link.'/'.$it->id.'/detail');
                            }elseif(Request::segment(1) == 'en') {
                                $url = url('en/'.$it->link.'/'.$it->id.'/detail');
                            }elseif(Request::segment(1) == 'cn') {
                                $url = url('cn/'.$it->link.'/'.$it->id.'/detail');
                            }else {
                                $url = url($it->link.'/'.$it->id.'/detail');
                            }
                        ?>
                     <li class="col-md-3 results-row">
                        <div class="product">
                           <a href="{{$url}}">
                              <div class="product-header">
                                  <span class="badge badge-success">{{$it->discount}}% OFF</span>
                                 <img class="img-fluid" src="{{url('images/'.$it->image)}}" alt="">
                                 @if($it->recode_level == '1')
                                 <span class="veg text-success mdi mdi-circle"></span>
                                 @else
                                 <span class="non-veg text-danger mdi mdi-circle"></span>
                                 @endif
                              </div>
                              <div class="product-body">
                                 <h5>{{$it->title}}</h5>
                                 <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6>
                              </div>
                              <div class="product-footer">
                                 <button type="button" class="btn btn-secondary btn-sm float-right">View More</button>
                                 <p class="offer-price mb-0 price" >${{$it->sell_price}} <i class="mdi mdi-tag-outline"></i><br><span class="regular-price">${{$it->cost_price}}</span></p>
                              </div>
                           </a>
                        </div>
                     </li>
                     @endif
                    @endforeach
                    </div>
                    <nav>
                  <ul class="pagination justify-content-center mt-4">
                  <?php 
								echo $txt;
								?>
                  </ul>
                  </nav>
                  </div>
               </ul>
            </div>
         </div>
      </section>
      @else
      <section class="shop-list section-padding" style="background-color:#fff;">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="notice notice-danger">
                     <strong>Nothing Found!</strong>Please refine your search and try again.
               </div>
               </div>
            </div>
         </div>
      </section>
               
      @endif   
<script>
   var ascending = false;

   $('.tab-content').on('click','.sortByPrice',function(){

      var sorted = $('.results-row').sort(function(a,b){
         return (ascending ==
                  (convertToNumber($(a).find('.price').html()) < 
                  convertToNumber($(b).find('.price').html()))) ? 1 : -1;
      });
      ascending = ascending ? false : true;

      $('.results').html(sorted);
   });
   var convertToNumber = function(value){
      return parseFloat(value.replace('$',''));
   }
</script>

<script type="text/javascript">
        
        function sortUnorderedList(ul, sortDescending) {
          if(typeof ul == "string")
            ul = document.getElementById(ul);

          var lis = ul.getElementsByTagName("LI");
          var vals = [];

          for(var i = 0, l = lis.length; i < l; i++)
            vals.push(lis[i].innerHTML);
            vals.sort();

          if(sortDescending)
            vals.reverse();

          for(var i = 0, l = lis.length; i < l; i++)
            lis[i].innerHTML = vals[i];
        }
        
        window.onload = function() {
          var desc = false;
          document.getElementById("test").onclick = function() {
            sortUnorderedList("list", desc);
            desc = !desc;
            return false;
          }
        }
        
</script>

@endsection