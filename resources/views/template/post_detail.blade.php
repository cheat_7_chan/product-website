
@extends('template.layout.master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{url('css/style-zoom.css')}}">
<section class="shop-single section-padding pt-3">
         <div class="container">
            <div class="row">
               <div class="col-md-6">

                  <div class="show" href="{{url('images/'.$data->image)}}">
                     <img src="{{url('images/'.$data->image)}}" id="show-img">
                  </div>

                  <!-- Secondary carousel image thumbnail gallery -->

                  <div class="small-img">
                     <img src="{{url('images/next-icon.png')}}" class="icon-left" alt="" id="prev-img">
                     <div class="small-container">
                     <div id="small-img-roll">
                        @foreach($data->image_item as $t)
                        <img src="{{url('images/'.$t->images)}}" class="show-small-img" alt="">
                        @endforeach
                     </div>
                     </div>
                     <img src="{{url('images/next-icon.png')}}" class="icon-right" alt="" id="next-img">
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="shop-detail-right">
                     <span class="badge badge-success">{{ $data->discount }}% OFF</span>
                     <h2>{{$data->title}}</h2>
                     <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6>
                     <p class="regular-price"><i class="mdi mdi-tag-outline"></i> MRP : ${{$data->cost_price}}</p>
                     <p class="offer-price mb-0">Discounted price : <span class="text-success">${{$data->sell_price}}</span></p>
                    
                     <div class="short-description">
                        <h5>
                           Quick Overview  
                           <p class="float-right">Availability: 
                              @if($data->recode_level == '1')
                              <span class="badge badge-success">In Stock</span>
                              @else
                              <span class="red_badge badge-danger">Out Stock</span>
                              @endif
                           </p>
                        </h5>
                        
                        <p class="mb-0">{!! $data->description !!}</p>
                     </div>
                     <div class="footer-social" style="margin-top:12px;"><span>Share : </span>
                     <?php $social = App\Social::get(); ?>
                        <!-- @foreach($social as $so)
                           <a href="{{url($so->link)}}"><i class="{{$so->class}}"></i></a>
                        @endforeach -->
                        <a target="_blank" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fdemo.titbserver.com/osahan/public/{{$data->title}}/{{$data->id}}/detail" title="Facebook Share"><img src="{{url('images/social/fb.png')}}"/></a>
                        <!-- <a class="btn-facebook" target="_blank" title="Click to share" href="http://www.facebook.com/sharer.php?u=http://demo.titbserver.com/osahan/public/{{$data->title}}/{{$data->id}}/detail"><i class="mdi mdi-facebook"></i></a>
                        <a class="btn-twitter" target="_blank" title="Click to post to Twitter" href="http://twitter.com/share?text=An%20intersting%20blog&url=http://demo.titbserver.com/osahan/public/{{$data->title}}/{{$data->id}}/detail"><i class="mdi mdi-twitter"></i></a>
                        <a class="btn-google" target="_blank" title="Click to share" href="https://plus.google.com/share?url=http://demo.titbserver.com/osahan/public/{{$data->title}}/{{$data->id}}/detail"><i class="mdi mdi-google"></i></a> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT"
        crossorigin="anonymous"></script>
     <script src="{{url('js/zoom-image.js')}}"></script>
     <script src="{{url('js/main.js')}}"></script>
@endsection
