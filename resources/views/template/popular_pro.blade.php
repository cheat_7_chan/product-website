<?php $cat_item = App\Category::where('block','=','popular_pro')->get(); ?>
<section class="product-items-slider section-padding">
         <div class="container">
            <div class="section-header">
               <h5 class="heading-design-h5">Popular
               </h5>
            </div>

            <div class="col-md-12">
                <div class="shop-head">
                   <div class="btn-group float-right mt-2">
                     <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     Sort by Products &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     </button>
                     <div class="dropdown-menu dropdown-menu-right">
                        @foreach($cat_item as $cat)
                        <a class="dropdown-item" href="shop.html#">{{$cat->name}}</a>
                        @endforeach
                     </div>
                  </div>
                  <h5 class="mb-3">Herbs</h5>
               </div><br>
               <div class="row no-gutters">
                @foreach($cat_item as $it)
                <?php 
                     $post = App\Item::get();

                     $pos = $it->item_post()->paginate(8);
                     $len = $pos->lastPage(); 
                     $txt = '';

                     for($i= 1 ; $i <=  $len; $i++){
                     if($pos->currentPage() == $i){
                        $active='active';
                     }else{
                        $active='';
                     } 
                     $txt .= '<li class="page-item '.$active.'"><a class="page-link" href="'.request()->url().'?page='.$i.'">'.$i.'</a></li>';
                     }?>
                @foreach($pos as $s)
                  @if($s->status ==1)
                        <?php
                            if(Request::segment(1) == 'kh') {
                                $url = url('kh/'.$s->link.'/'.$s->id.'/detail');
                            }elseif(Request::segment(1) == 'en') {
                                $url = url('en/'.$s->link.'/'.$s->id.'/detail');
                            }elseif(Request::segment(1) == 'cn') {
                                $url = url('cn/'.$s->link.'/'.$s->id.'/detail');
                            }else {
                                $url = url($s->link.'/'.$s->id.'/detail');
                            }
                        ?>
                  <div class="col-md-3">
                     <div class="product">
                        @if($s->link)
                        <a href="{{$url}}">
                           <div class="product-header">
                              <img class="img-fluid" src="{{url('images/'.$s->image)}}" alt="">
                           </div>
                           <div class="product-body">
                           <h5>{{$s->title}}</h5>
                           <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6>
                           </div>

                           <div class="product-footer">
                              <button type="button" class="btn btn-secondary btn-sm float-right">View More</button>
                              <p class="offer-price mb-0">${{$s->sell_price}} <i class="mdi mdi-tag-outline"></i><br><span class="regular-price">${{$s->cost_price}}</span></p>
                           </div>
                        </a>
                        @else
                        <a href="#">
                           <div class="product-header">
                              <img class="img-fluid" src="{{url('images/'.$s->image)}}" alt="">
                           </div>
                           <div class="product-body">
                           <h5>{{$s->title}}</h5>
                              <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6>
                           </div>

                           <div class="product-footer">
                              <button type="button" class="btn btn-secondary btn-sm float-right">View More</button>
                              <p class="offer-price mb-0">${{$s->sell_price}}<i class="mdi mdi-tag-outline"></i><br><span class="regular-price">${{$s->cost_price}}</span></p>
                           </div>
                        </a>
                        @endif
                     </div>
                  </div>
                  @endif
                @endforeach
                @endforeach
               </div>
               <nav>
                  <ul class="pagination justify-content-center mt-4">
                     <?php 
								echo $txt;
								?>
                  </ul>
               </nav>
            </div>
            
         </div>
      </section>