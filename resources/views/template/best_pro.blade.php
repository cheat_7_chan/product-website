<?php $cat = App\Category::where('block','=','best_pro')->get(); ?>
@foreach($cat as $val)
<section class="product-items-slider section-padding">
         <div class="container">
         

            <div class="section-header">
             
               <h5 class="heading-design-h5">{{$val->name}}<span class="badge badge-dark">20% OFF</span>
                  <a class="float-right text-white" href="shop.html">View All</a>
               </h5>
           
            
            </div>
            <div class="owl-carousel owl-carousel-featured owl-theme" style="opacity: 1; display: block;">
               <div class="owl-wrapper-outer">
             
                  <div class="owl-wrapper" style="width: 2664px; left: 0px; display: block;">
                  
                             
                              @foreach($val->item_post as $it)
                              <?php
                                 if(Request::segment(1) == 'kh') {
                                    $url = url('kh/'.$it->link.'/'.$it->id.'/detail');
                                 }elseif(Request::segment(1) == 'en') {
                                    $url = url('en/'.$it->link.'/'.$it->id.'/detail');
                                 }elseif(Request::segment(1) == 'cn') {
                                    $url = url('cn/'.$it->link.'/'.$it->id.'/detail');
                                 }else {
                                    $url = url($it->link.'/'.$it->id.'/detail');
                                 }
                              ?>
                              <div class="owl-item" style="width: 222px;">
                                 <div class="item">
                                    <div class="product">
                                       <a href="{{$url}}">
                                          <div class="product-header">
                                             <img class="img-fluid" src="{{url('images/'.$it->image)}}" alt="">
                                          </div>
                                          <div class="product-body">
                                             <h5>{{$it->title}}</h5>
                                             <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6>
                                          </div>
                                          <div class="product-footer">
                                             <button type="button" class="btn btn-secondary btn-sm float-right">View More</button>
                                             <p class="offer-price mb-0">${{$it->sell_price}}<i class="mdi mdi-tag-outline"></i><br><span class="regular-price">${{$it->cost_price}}</span></p>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              
                           @endforeach
                        
                     
                      
                  </div>
               </div>
              
            <!-- <div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"><i class="mdi mdi-chevron-left"></i></div><div class="owl-next"><i class="mdi mdi-chevron-right"></i></div></div></div></div> -->
         </div>
       
         </div>
      </section>
@endforeach
