<section class="section-padding">
         <div class="container">
            <div class="row">
               <div class="col-lg-4 col-md-4">
                  <h3 class="mt-1 mb-5">{{$cat->name}}</h3>
                  <?php $set = App\Setting::get(); ?>
                  @foreach($set as $s)
                  <h6 class="text-dark"><i class="mdi mdi-home-map-marker"></i> Address :</h6>
                  <p>{{ strip_tags($s->address)}}</p>
                  <h6 class="text-dark"><i class="mdi mdi-phone"></i> Phone :</h6>
                  <p>{{ $s->phone}}</p>
                  <!-- <h6 class="text-dark"><i class="mdi mdi-deskphone"></i> Mobile :</h6>
                  <p>(+20) 220 145 6589, +91 12345-67890</p> -->
                  <h6 class="text-dark"><i class="mdi mdi-email"></i> Email :</h6>
                  <p>{{$s->email}}</p>
                  <h6 class="text-dark"><i class="mdi mdi-link"></i> Website :</h6>
                  <p>{{$s->website_url}}</p>
                  @endforeach
                  <div class="footer-social"><span>Follow : </span>
                  <?php $social = App\Social::get(); ?>
                    @foreach($social as $so)
                     <a href="{{url($so->link)}}"><i class="{{$so->class}}"></i></a>
                    @endforeach
                  </div>
               </div>
               <div class="col-lg-8 col-md-8">
                  <div class="card">
                     @foreach($set as $s)
                     <div class="card-body">
                        {!! $s->address_site !!}
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </div>
      </section>