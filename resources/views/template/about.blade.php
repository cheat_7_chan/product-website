
<section class="section-padding bg-white">
    <div class="container">
        <div class="row">
            @foreach($cat->post as $s)
            <div class="pl-4 col-lg-5 col-md-5 pr-4">
                @if($s->image == true)
                <img class="rounded img-fluid" src="{{url('images/'.$s->image)}}" >
                @else

                @endif
            </div>
            <div class="col-lg-6 col-md-6 pl-5 pr-5">
                <h2 class="mt-5 mb-5 text-secondary">{{ $s->title }}</h2>
                <p>{!! $s->description !!}</p>
            </div>
            @endforeach
        </div>
    </div>
</section>