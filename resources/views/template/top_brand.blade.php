<section class="top-brands">
         <div class="container">
            <div class="section-header">
               <h5 class="heading-design-h5">Top Brands <span class="badge badge-primary">200 Brands</span></h5>
            </div>
            <div class="row text-center">
               <div class="col-lg-2 col-md-2 col-sm-2">
                  <a href="#"><img class="img-responsive" src="images/brand1.jpg" alt=""></a>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                  <a href="#"><img class="img-responsive" src="images/brand2.jpg" alt=""></a>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                  <a href="#"><img class="img-responsive" src="images/brand3.jpg" alt=""></a>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                  <a href="#"><img class="img-responsive" src="images/brand4.jpg" alt=""></a>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                  <a href="#"><img class="img-responsive" src="images/brand5.jpg" alt=""></a>
               </div>
               <div class="col-lg-2 col-md-2 col-sm-2">
                  <a href="#"><img class="img-responsive" src="images/brand6.jpg" alt=""></a>
               </div>
            </div>
         </div>
      </section>