
<?php   
    $setting= App\Setting::where('language',$local)->get();
    $widget = App\Widget::where('language',$local)->get();
?>
<div class="col-lg-3 col-md-3">
@foreach($setting as $set)
    <h4 class="mb-5 mt-0"><a class="logo" href="index.html"><img src="{{url('images/logo-footer.png')}}" alt="Osahan Grocery"></a></h4>
    <p class="mb-0"><a class="text-dark" href="my-profile.html#"><i class="mdi mdi-phone"></i>{{$set->phone}}</a></p>
    <p class="mb-0"><a class="text-success" href="my-profile.html#"><i class="mdi mdi-email"></i>{{$set->email}}</a></p>
    <p class="mb-0"><a class="text-primary" href="my-profile.html#"><i class="mdi mdi-web"></i> {{$set->website_url}}</a></p>
@endforeach
</div>