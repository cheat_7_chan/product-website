<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php $cat_item = App\Category::where('block','=','herbs')->get(); ?>
@foreach($cat_item as $it)
<section class="shop-list section-padding">
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div class="shop-filters">
                     <div id="accordion">
                        <?php $cat = App\Item_Cat::where('category_type','=','product')->get(); ?>
                        @if(count($cat)>0)
                        @foreach($cat as $cate)
                        <div class="card">
                           <div class="card-header">
                              <h5 class="mb-0">
                                 <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#{{$cate->link}}" aria-expanded="false" aria-controls="{{$cate->link}}">
                                 {{$cate->name}}<span class="mdi mdi-chevron-down float-right"></span>
                                 </button>
                              </h5>
                           </div>
                           <div id="{{$cate->link}}" class="collapse"  data-parent="#accordion">
                              <div class="card-body">
                                 <div class="list-group">
                                    <a href="shop.html#" class="list-group-item list-group-item-action">All Fruits</a>
                                   
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>
                  </div>
                  <div class="left-ad mt-4">
				  <img class="img-fluid" src="http://via.placeholder.com/254x444" alt="">
				  </div>
               </div>
               <div class="col-md-9">
                  <a href="{{url('fruits-vegetables')}}"><img class="img-fluid mb-3" src="images/shop.jpg" alt=""></a>
                  <div class="shop-head">
                     <a href="{{url('home')}}"><span class="mdi mdi-home"></span> Home</a> <span class="mdi mdi-chevron-right"></span> <a href="{{url('fruits-vegetables')}}">Fruits &amp; Vegetables</a> <span class="mdi mdi-chevron-right"></span> <a href="{{url('fruits-vegetables')}}">Fruits</a>
                     <div class="btn-group float-right mt-2  tab-content">
                        <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sort by Products &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                        <div class="dropdown-menu dropdown-menu-right"  id="filters">
                           <a class="dropdown-item sortByPrice" href="#">Price (Low to High)</a>
                           <a class="dropdown-item sortByPrice" href="#">Price (High to Low)</a>
                           <a class="dropdown-item sortByName" href="#" id="test">Name (A to Z)</a>
                        </div>
                     </div>
                     <h5 class="mb-3">{{$it->name}}</h5>
                  </div>
                  <div class="tab-pane active ">
                    <ul class="row no-gutters  results" id="list">
                <?php 
                     $post = App\Item::get();

                     $pos = $it->item_post()->paginate(9);
                     $len = $pos->lastPage(); 
                     $txt = '';

                     for($i= 1 ; $i <=  $len; $i++){
                     if($pos->currentPage() == $i){
                        $active='active';
                     }else{
                        $active='';
                     } 
                     $txt .= '<li class="page-item '.$active.'"><a class="page-link" href="'.request()->url().'?page='.$i.'">'.$i.'</a></li>';
                     }?>
                @foreach($pos as $s)
                  @if($s->status ==1)
                        <?php
                            if(Request::segment(1) == 'kh') {
                                $url = url('kh/'.$s->link.'/'.$s->id.'/detail');
                            }elseif(Request::segment(1) == 'en') {
                                $url = url('en/'.$s->link.'/'.$s->id.'/detail');
                            }elseif(Request::segment(1) == 'cn') {
                                $url = url('cn/'.$s->link.'/'.$s->id.'/detail');
                            }else {
                                $url = url($s->link.'/'.$s->id.'/detail');
                            }
                        ?>
                     <li class="col-md-4 results-row">
                        <div class="product">
                           <a href="{{$url}}">
                              <div class="product-header">
                                  <span class="badge badge-success">{{$s->discount}}% OFF</span>
                                 <img class="img-fluid" src="{{url('images/'.$s->image)}}" alt="">
                                 @if($s->recode_level == '1')
                                 <span class="veg text-success mdi mdi-circle"></span>
                                 @else
                                 <span class="non-veg text-danger mdi mdi-circle"></span>
                                 @endif
                              </div>
                              <div class="product-body">
                                 <h5>{{$s->title}}</h5>
                                 <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6>
                              </div>
                              <div class="product-footer">
                                 <button type="button" class="btn btn-secondary btn-sm float-right">View More</button>
                                 <p class="offer-price mb-0 price" >${{$s->sell_price}} <i class="mdi mdi-tag-outline"></i><br><span class="regular-price">${{$s->cost_price}}</span></p>
                              </div>
                           </a>
                        </div>
                     </li>
                     @endif
                    @endforeach
                    </div>
                    <nav>
                  <ul class="pagination justify-content-center mt-4">
                     <?php 
								echo $txt;
								?>
                  </ul>
                  </nav>
                  </div>
               </ul>
            </div>
         </div>
      </section>
 @endforeach
 <script>
   var ascending = false;

$('.tab-content').on('click','.sortByPrice',function(){

    var sorted = $('.results-row').sort(function(a,b){
        return (ascending ==
               (convertToNumber($(a).find('.price').html()) < 
                convertToNumber($(b).find('.price').html()))) ? 1 : -1;
    });
    ascending = ascending ? false : true;

    $('.results').html(sorted);
});

var convertToNumber = function(value){
     return parseFloat(value.replace('$',''));
}
</script>

<script type="text/javascript">
        
        function sortUnorderedList(ul, sortDescending) {
          if(typeof ul == "string")
            ul = document.getElementById(ul);

          var lis = ul.getElementsByTagName("LI");
          var vals = [];

          for(var i = 0, l = lis.length; i < l; i++)
            vals.push(lis[i].innerHTML);
            vals.sort();

          if(sortDescending)
            vals.reverse();

          for(var i = 0, l = lis.length; i < l; i++)
            lis[i].innerHTML = vals[i];
        }
        
        window.onload = function() {
          var desc = false;
          document.getElementById("test").onclick = function() {
            sortUnorderedList("list", desc);
            desc = !desc;
            return false;
          }
        }
        
</script>