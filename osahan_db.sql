-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 21, 2020 at 04:02 AM
-- Server version: 5.7.26
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `osahan_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) DEFAULT NULL,
  `text` text,
  `link_fb` text,
  `link_yt` text,
  `layout` varchar(250) DEFAULT NULL,
  `style` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ads_images`
--

DROP TABLE IF EXISTS `ads_images`;
CREATE TABLE IF NOT EXISTS `ads_images` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `ads_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `language` varchar(220) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `address` text,
  `img` varchar(255) DEFAULT NULL,
  `category_type` varchar(255) DEFAULT NULL,
  `category_products` int(11) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `description` text,
  `status` int(10) DEFAULT NULL,
  `num_of_staff` varchar(60) DEFAULT NULL,
  `block` varchar(60) DEFAULT NULL,
  `google_embed` text,
  `deleted` int(10) DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `unpublish_date` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `language`, `phone`, `email`, `address`, `img`, `category_type`, `category_products`, `user_id`, `description`, `status`, `num_of_staff`, `block`, `google_embed`, `deleted`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`) VALUES
(9, 0, 'Contact Us', '1', NULL, NULL, NULL, NULL, 'category', NULL, 11, '', 1, NULL, 'contact_us', NULL, NULL, NULL, NULL, '2020-03-09', NULL),
(8, 0, 'Get In Touch', '1', NULL, NULL, NULL, NULL, 'category', NULL, 11, '', 1, NULL, 'contact', NULL, NULL, NULL, NULL, '2020-03-09', NULL),
(7, 0, 'About us', '1', NULL, NULL, NULL, NULL, 'category', NULL, 11, '', 1, NULL, 'about', NULL, NULL, NULL, NULL, '2020-03-09', NULL),
(24, 0, 'Popular', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'popular_pro', NULL, NULL, NULL, NULL, '2020-03-19', '2020-03-19'),
(18, 0, 'Top Savers Today', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'product_list', NULL, NULL, NULL, NULL, '2020-03-19', '2020-03-19'),
(25, 0, 'Best Offers View', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'product_list', NULL, NULL, NULL, NULL, '2020-03-20', NULL),
(26, 0, 'Herbs', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'herbs', NULL, NULL, NULL, NULL, '2020-03-20', '2020-03-20'),
(27, 0, 'Best Products View', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'best_pro', NULL, NULL, NULL, NULL, '2020-03-20', NULL),
(28, 0, 'Fruits', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'fruits', NULL, NULL, NULL, NULL, '2020-03-20', '2020-03-20'),
(29, 0, 'Best Fruits View', '1', NULL, NULL, NULL, NULL, 'category', 1, 11, '', 1, NULL, 'best_fruits', NULL, NULL, NULL, NULL, '2020-03-20', '2020-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `cat_galleries`
--

DROP TABLE IF EXISTS `cat_galleries`;
CREATE TABLE IF NOT EXISTS `cat_galleries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `footers`
--

DROP TABLE IF EXISTS `footers`;
CREATE TABLE IF NOT EXISTS `footers` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `link` varchar(250) DEFAULT NULL,
  `address` text,
  `status` int(5) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `footer_pages`
--

DROP TABLE IF EXISTS `footer_pages`;
CREATE TABLE IF NOT EXISTS `footer_pages` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `page_id` int(5) DEFAULT NULL,
  `footer_id` int(5) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `galleries_image`
--

DROP TABLE IF EXISTS `galleries_image`;
CREATE TABLE IF NOT EXISTS `galleries_image` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) NOT NULL,
  `link` varchar(200) DEFAULT NULL,
  `gallerie_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `category_galleries_id` int(10) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `link_download` varchar(255) DEFAULT NULL,
  `item_code` varchar(100) DEFAULT NULL,
  `item_bacode` varchar(100) DEFAULT NULL,
  `language` varchar(200) DEFAULT NULL,
  `category_id` int(100) DEFAULT NULL,
  `brand_id` int(100) DEFAULT NULL,
  `branch_id` int(100) DEFAULT NULL,
  `user_id` int(100) DEFAULT NULL,
  `supplier_id` int(100) DEFAULT NULL,
  `items_category_id` int(100) DEFAULT NULL,
  `qty` int(100) DEFAULT NULL,
  `post_type` varchar(200) DEFAULT NULL,
  `tax_include` int(100) DEFAULT NULL,
  `commission_fixed` float(13,2) DEFAULT NULL,
  `commission_give` float(13,2) DEFAULT NULL,
  `commission_type` varchar(50) DEFAULT '0',
  `override_default_tax` float DEFAULT NULL,
  `override_default_commission` int(100) DEFAULT NULL,
  `cost_price` float(13,2) DEFAULT NULL,
  `sell_price` float(13,2) DEFAULT NULL,
  `promo_price` float(13,2) DEFAULT NULL,
  `location_id` int(11) DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `discount_percent` int(11) DEFAULT NULL,
  `discount` int(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `description` text,
  `is_service` int(100) DEFAULT NULL,
  `quality` int(100) DEFAULT NULL,
  `status` int(100) DEFAULT '1',
  `recode_level` int(100) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `long_description` text,
  `is_feature` int(11) DEFAULT '0',
  `is_special` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `title`, `link`, `link_download`, `item_code`, `item_bacode`, `language`, `category_id`, `brand_id`, `branch_id`, `user_id`, `supplier_id`, `items_category_id`, `qty`, `post_type`, `tax_include`, `commission_fixed`, `commission_give`, `commission_type`, `override_default_tax`, `override_default_commission`, `cost_price`, `sell_price`, `promo_price`, `location_id`, `start_date`, `end_date`, `discount_percent`, `discount`, `image`, `size`, `description`, `is_service`, `quality`, `status`, `recode_level`, `deleted`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`, `long_description`, `is_feature`, `is_special`) VALUES
(2, 'Product Title Here', 'product-title-here', '', NULL, NULL, '1', 12, NULL, NULL, 11, NULL, 18, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '1 - Copy.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-13 00:00:00', '2020-04-13 07:45:37', NULL, 0, 0),
(3, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 12, NULL, NULL, 11, NULL, 18, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '2 - Copy - Copy.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:22:09', NULL, 0, 0),
(4, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 12, NULL, NULL, 11, NULL, 18, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '5 - Copy - Copy - Copy.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, NULL, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:23:36', NULL, 0, 0),
(5, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 12, NULL, NULL, 11, NULL, 18, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '6 - Copy.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:27:54', NULL, 0, 0),
(6, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 12, NULL, NULL, 11, NULL, 18, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '4.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, NULL, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:31:36', NULL, 0, 0),
(7, 'Product Title Here', 'product-title-here', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '1 - Copy.webp', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-14 00:00:00', '2020-04-14 06:49:50', NULL, 0, 0),
(8, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herbs2.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-14 00:00:00', '2020-04-14 06:51:11', NULL, 0, 0),
(9, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herbs3.png', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-14 00:00:00', '2020-04-14 07:07:52', NULL, 0, 0),
(10, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb6.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-14 00:00:00', '2020-04-14 07:21:49', NULL, 0, 0),
(11, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 19, NULL, NULL, 11, NULL, 25, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '11.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:42:09', NULL, 0, 0),
(12, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 19, NULL, NULL, 11, NULL, 25, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '10.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:39:51', NULL, 0, 0),
(13, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 19, NULL, NULL, 11, NULL, 25, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '9.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:37:51', NULL, 0, 0),
(14, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 19, NULL, NULL, 11, NULL, 25, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '8 - Copy (2).jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:34:48', NULL, 0, 0),
(15, 'Product Title Here', 'product-title-here', '', NULL, NULL, '1', 19, NULL, NULL, 11, NULL, 25, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '7.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-13 00:00:00', '2020-04-13 08:33:46', NULL, 0, 0),
(16, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb7.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-14 00:00:00', '2020-04-14 07:26:30', NULL, 0, 0),
(17, 'Product Title Here 5', 'product-title-here-5', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'home_verticaltabs_2.webp', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-14 00:00:00', '2020-04-14 07:32:30', NULL, 0, 0),
(18, 'Product Title Here 6', 'product-title-here-6', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herbs3.png', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-04-14 00:00:00', '2020-04-14 07:40:34', NULL, 0, 0),
(19, 'Product Title Here 7', 'product-title-here-7', '', NULL, NULL, '1', 20, NULL, NULL, 11, NULL, 24, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb6.jpg', NULL, '<p>Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>\r\n', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-04-14 00:00:00', '2020-04-14 07:47:52', NULL, 0, 0),
(20, 'Product Title Here', 'product-title-here', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 0, 'herbs2.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(21, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 0, '1 - Copy.webp', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(22, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herbs3.png', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-20 00:00:00', '2020-03-20 04:48:28', NULL, 0, 0),
(23, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb7.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(24, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb4.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(25, 'Product Title Here 5', 'product-title-here-5', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'home_verticaltabs_2.webp', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(26, 'Product Title Here 6', 'product-title-here-6', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb6.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(27, 'Product Title Here 7', 'product-title-here-7', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herbs2.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(28, 'Product Title Here 8', 'product-title-here-8', '', NULL, NULL, '1', 21, NULL, NULL, 11, NULL, 26, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb4.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(29, 'Product Title Here', 'product-title-here', '', NULL, NULL, '1', 22, NULL, NULL, 11, NULL, 27, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '1.webp', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-20 00:00:00', '2020-03-20 05:09:32', NULL, 0, 0),
(30, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 22, NULL, NULL, 11, NULL, 27, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herb6.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(31, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 22, NULL, NULL, 11, NULL, 27, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, 'herbs2.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(32, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 22, NULL, NULL, 11, NULL, 27, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 0, 'herb7.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(33, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 22, NULL, NULL, 11, NULL, 27, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 0, 'herbs3.png', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(34, 'Product Title Here ', 'product-title-here', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '1 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(35, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 400.99, NULL, 0, NULL, NULL, NULL, 50, '2 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(36, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 350.99, NULL, 0, NULL, NULL, NULL, 50, '3 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(37, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 320.99, NULL, 0, NULL, NULL, NULL, 50, '4.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(38, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 300.99, NULL, 0, NULL, NULL, NULL, 50, '5 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(39, 'Product Title Here 5', 'product-title-here-5', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 420.99, NULL, 0, NULL, NULL, NULL, 50, '6 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(40, 'Product Title Here 7', 'product-title-here-7', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 250.99, NULL, 0, NULL, NULL, NULL, 50, '8 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-20 00:00:00', '2020-03-20 07:06:03', NULL, 0, 0),
(41, 'Product Title Here 6', 'product-title-here-6', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 330.99, NULL, 0, NULL, NULL, NULL, 50, '7 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(42, 'Product Title Here 8', 'product-title-here-8', '', NULL, NULL, '1', 23, NULL, NULL, 11, NULL, 28, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 220.99, NULL, 0, NULL, NULL, NULL, 50, '9 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(43, 'Product Title Here', 'product-title-here', '', NULL, NULL, '1', 24, NULL, NULL, 11, NULL, 29, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '7 - Copy - Copy - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-20 00:00:00', '2020-03-20 07:37:07', NULL, 0, 0),
(44, 'Product Title Here 1', 'product-title-here-1', '', NULL, NULL, '1', 24, NULL, NULL, 11, NULL, 29, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '8 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(45, 'Product Title Here 2', 'product-title-here-2', '', NULL, NULL, '1', 24, NULL, NULL, 11, NULL, 29, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '9 - Copy - Copy - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(46, 'Product Title Here 3', 'product-title-here-3', '', NULL, NULL, '1', 24, NULL, NULL, 11, NULL, 29, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '10 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0),
(47, 'Product Title Here 4', 'product-title-here-4', '', NULL, NULL, '1', 24, NULL, NULL, 11, NULL, 29, NULL, 'product', NULL, NULL, NULL, '0', NULL, NULL, 800.99, 450.99, NULL, 0, NULL, NULL, NULL, 50, '12 - Copy.jpg', NULL, '<p class=\"mb-0\"> Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hiMenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum.</p>', NULL, NULL, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-20 00:00:00', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_categorys`
--

DROP TABLE IF EXISTS `item_categorys`;
CREATE TABLE IF NOT EXISTS `item_categorys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `block` varchar(200) DEFAULT NULL,
  `category_type` varchar(200) DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `post_id` int(50) DEFAULT NULL,
  `order` float DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(100) DEFAULT NULL,
  `images` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `user_id` int(20) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_categorys`
--

INSERT INTO `item_categorys` (`id`, `name`, `link`, `block`, `category_type`, `parent_id`, `post_id`, `order`, `description`, `status`, `images`, `user_id`, `deleted`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`) VALUES
(19, 'Best', 'best', NULL, 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-20 00:00:00', '2020-04-03 00:00:00'),
(20, 'Populars', 'populars', NULL, 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-20 00:00:00', NULL),
(12, 'Products', 'products', 'product_list', 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-11 00:00:00', '2020-03-19 00:00:00'),
(21, 'Herb', 'herb', NULL, 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-20 00:00:00', NULL),
(22, 'Best Products', 'best_products', NULL, 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-20 00:00:00', NULL),
(23, 'Fruit', 'fruit', NULL, 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-20 00:00:00', NULL),
(24, 'Best Fruit', 'best_fruit', NULL, 'product', 0, NULL, NULL, '', 1, NULL, 11, 0, NULL, NULL, '2020-03-20 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_categorys_page`
--

DROP TABLE IF EXISTS `item_categorys_page`;
CREATE TABLE IF NOT EXISTS `item_categorys_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `items_id` int(11) DEFAULT NULL,
  `items_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=389 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_categorys_page`
--

INSERT INTO `item_categorys_page` (`id`, `position`, `items_id`, `items_category_id`) VALUES
(334, NULL, 21, 26),
(333, NULL, 20, 26),
(388, NULL, 19, 24),
(387, NULL, 18, 24),
(386, NULL, 17, 24),
(385, NULL, 16, 24),
(376, NULL, 15, 25),
(377, NULL, 14, 25),
(378, NULL, 13, 25),
(379, NULL, 12, 25),
(380, NULL, 11, 25),
(384, NULL, 10, 24),
(383, NULL, 9, 24),
(382, NULL, 8, 24),
(381, NULL, 7, 24),
(375, NULL, 6, 18),
(374, NULL, 5, 18),
(373, NULL, 4, 18),
(371, NULL, 2, 18),
(372, NULL, 3, 18),
(336, NULL, 22, 26),
(337, NULL, 23, 26),
(338, NULL, 24, 26),
(339, NULL, 25, 26),
(340, NULL, 26, 26),
(341, NULL, 27, 26),
(342, NULL, 28, 26),
(344, NULL, 29, 27),
(345, NULL, 30, 27),
(346, NULL, 31, 27),
(347, NULL, 32, 27),
(348, NULL, 33, 27),
(349, NULL, 34, 28),
(350, NULL, 35, 28),
(351, NULL, 36, 28),
(352, NULL, 37, 28),
(353, NULL, 38, 28),
(354, NULL, 39, 28),
(357, NULL, 40, 28),
(356, NULL, 41, 28),
(358, NULL, 42, 28),
(360, NULL, 43, 29),
(361, NULL, 44, 29),
(362, NULL, 45, 29),
(363, NULL, 46, 29),
(364, NULL, 47, 29);

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

DROP TABLE IF EXISTS `item_images`;
CREATE TABLE IF NOT EXISTS `item_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `item_id` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=155 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_images`
--

INSERT INTO `item_images` (`id`, `images`, `item_id`) VALUES
(51, '2 - Copy - Copy.jpg', 3),
(49, '1 - Copy.jpg', 2),
(48, '2 - Copy - Copy.jpg', 2),
(47, '5 - Copy - Copy - Copy.jpg', 2),
(46, '6 - Copy.jpg', 2),
(45, '4.jpg', 2),
(50, '1 - Copy.jpg', 3),
(69, '4.jpg', 6),
(53, '4.jpg', 3),
(54, '5 - Copy - Copy - Copy.jpg', 3),
(55, '6 - Copy.jpg', 3),
(56, '5 - Copy - Copy.jpg', 4),
(57, '2 - Copy - Copy.jpg', 4),
(70, '2.jpg', 6),
(59, '1.jpg', 4),
(60, '4.jpg', 4),
(61, '6 - Copy.jpg', 4),
(62, '6 - Copy - Copy.jpg', 5),
(63, '2 - Copy - Copy.jpg', 5),
(68, '1 - Copy.jpg', 6),
(65, '4.jpg', 5),
(66, '5 - Copy - Copy.jpg', 5),
(67, '1.jpg', 5),
(71, '5 - Copy (2).jpg', 6),
(72, '6 - Copy.jpg', 6),
(73, '7 - Copy - Copy.jpg', 15),
(74, '8 - Copy.jpg', 15),
(75, '9 - Copy.jpg', 15),
(76, '10 - Copy.jpg', 15),
(77, '11 - Copy.jpg', 15),
(78, '8 - Copy.jpg', 14),
(79, '7 - Copy - Copy.jpg', 14),
(80, '9 - Copy - Copy - Copy.jpg', 14),
(81, '10 - Copy.jpg', 14),
(82, '11.jpg', 14),
(83, '9 - Copy.jpg', 13),
(84, '8 - Copy.jpg', 13),
(85, '7 - Copy - Copy.jpg', 13),
(86, '10 - Copy.jpg', 13),
(87, '11 - Copy.jpg', 13),
(88, '10 - Copy.jpg', 12),
(89, '8 - Copy.jpg', 12),
(91, '9 - Copy.jpg', 12),
(92, '7 - Copy - Copy.jpg', 12),
(93, '11 - Copy.jpg', 12),
(94, '11 - Copy.jpg', 11),
(95, '8 - Copy.jpg', 11),
(96, '9 - Copy - Copy.jpg', 11),
(97, '10 - Copy.jpg', 11),
(98, '7 - Copy - Copy.jpg', 11),
(99, '1 - Copy.webp', 7),
(100, 'herb4.jpg', 7),
(101, 'herb6.jpg', 7),
(102, 'herb7.jpg', 7),
(103, 'herbs2.jpg', 7),
(104, 'herbs3.png', 7),
(105, 'home_verticaltabs_2.webp', 7),
(106, 'herbs2.jpg', 8),
(107, 'herb4.jpg', 8),
(108, 'herb6.jpg', 8),
(109, 'herb7.jpg', 8),
(110, 'home_verticaltabs_2.webp', 8),
(111, 'herbs3.png', 8),
(112, '1.webp', 8),
(113, 'herbs3.png', 9),
(114, 'herb4.jpg', 9),
(115, 'herb6.jpg', 9),
(116, 'herb7.jpg', 9),
(117, 'herbs2.jpg', 9),
(118, '1.webp', 9),
(119, 'home_verticaltabs_2.webp', 9),
(120, 'herb6.jpg', 10),
(121, '1.webp', 10),
(122, 'herb4.jpg', 10),
(123, 'herb7.jpg', 10),
(124, 'herbs2.jpg', 10),
(125, 'herbs3.png', 10),
(126, 'home_verticaltabs_2.webp', 10),
(127, 'herb7.jpg', 16),
(128, 'herb4.jpg', 16),
(129, 'herb6.jpg', 16),
(130, '1.webp', 16),
(131, 'herbs2.jpg', 16),
(132, 'herbs3.png', 16),
(133, 'home_verticaltabs_2.webp', 16),
(134, 'home_verticaltabs_2.webp', 17),
(135, 'herb4.jpg', 17),
(136, 'herb6.jpg', 17),
(137, 'herb7.jpg', 17),
(138, 'herbs2.jpg', 17),
(139, 'herbs3.png', 17),
(140, '1.webp', 17),
(141, 'herbs3.png', 18),
(142, 'herb4.jpg', 18),
(143, 'herb6.jpg', 18),
(144, 'herb7.jpg', 18),
(145, 'herbs2.jpg', 18),
(146, '1.webp', 18),
(147, 'home_verticaltabs_2.webp', 18),
(148, 'herb6.jpg', 19),
(149, 'herb4.jpg', 19),
(150, '1.webp\r\n', 19),
(151, 'herb7.jpg', 19),
(152, 'herbs2.jpg', 19),
(153, 'herbs3.png', 19),
(154, 'home_verticaltabs_2.webp', 19);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `image` text,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `zipcode`, `code`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', '', '', 'logo1.png', 1, NULL, NULL),
(2, 'khmer', '', '', 'logo2.png', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `parent_id` int(10) DEFAULT '0',
  `link` varchar(250) DEFAULT NULL,
  `language` varchar(220) DEFAULT NULL,
  `menu_type_id` int(11) DEFAULT NULL,
  `ordering` float(10,2) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `modul_class` varchar(255) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `parent_id`, `link`, `language`, `menu_type_id`, `ordering`, `image`, `status`, `user_id`, `modul_class`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`, `deleted`) VALUES
(4, 'Fruits & Vegetables', 0, 'fruits-vegetables', '1', 1, 3.00, '', 1, 11, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2020-02-28', NULL, 1),
(3, 'Herbs', 0, 'herbs', '1', 1, 2.00, '', 1, 11, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2020-02-28', NULL, 1),
(2, 'About Us', 0, 'about-us', '1', 1, 1.00, '', 1, 11, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2020-02-28', NULL, 1),
(1, 'Home', 0, 'home', '1', 1, 0.00, NULL, 0, 11, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2020-02-28', '2020-04-03', 1),
(5, 'Contact', 0, 'contact', '1', 1, 4.00, '', 1, 11, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2020-02-28', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_posts`
--

DROP TABLE IF EXISTS `menu_posts`;
CREATE TABLE IF NOT EXISTS `menu_posts` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `menu_id` int(5) NOT NULL,
  `post_id` int(5) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_posts`
--

INSERT INTO `menu_posts` (`id`, `menu_id`, `post_id`, `created_at`, `updated_at`) VALUES
(24, 5, 5, NULL, NULL),
(55, 4, 4, NULL, NULL),
(53, 3, 3, NULL, NULL),
(22, 2, 2, NULL, NULL),
(49, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_types`
--

DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE IF NOT EXISTS `menu_types` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_types`
--

INSERT INTO `menu_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Main Menu', '2020-02-27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_categories`
--

DROP TABLE IF EXISTS `page_categories`;
CREATE TABLE IF NOT EXISTS `page_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_categories`
--

INSERT INTO `page_categories` (`id`, `page_id`, `categorie_id`, `company_id`, `brand_id`, `created_at`, `updated_at`) VALUES
(44, 1, NULL, NULL, 0, NULL, NULL),
(45, 6, 7, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `link` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `description` text,
  `template` varchar(200) DEFAULT NULL,
  `count` int(5) DEFAULT NULL,
  `language` varchar(220) DEFAULT NULL,
  `link_download` varchar(400) DEFAULT NULL,
  `post_type` varchar(200) DEFAULT NULL,
  `post_format` varchar(220) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `is_againt` int(11) DEFAULT NULL,
  `deleted` int(10) DEFAULT NULL,
  `event_start_date` datetime DEFAULT NULL,
  `event_end_date` datetime DEFAULT NULL,
  `s_from_rang` float DEFAULT NULL,
  `s_to_rang` float DEFAULT NULL,
  `location_job` varchar(60) DEFAULT NULL,
  `job_type` varchar(60) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `link`, `image`, `description`, `template`, `count`, `language`, `link_download`, `post_type`, `post_format`, `user_id`, `status`, `is_againt`, `deleted`, `event_start_date`, `event_end_date`, `s_from_rang`, `s_to_rang`, `location_job`, `job_type`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', NULL, '', '', NULL, '1', NULL, 'page', NULL, 11, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2020-03-19', '2020-04-03'),
(2, 'About Us', 'about-us', NULL, '', '', NULL, '1', NULL, 'page', NULL, 11, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-09', '2020-03-09'),
(3, 'Herbs', 'herbs', NULL, '', '', NULL, '1', NULL, 'page', NULL, 11, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-20', '2020-03-20'),
(4, 'Fruits & Vegetables', 'fruits-vegetables', NULL, '', '', NULL, '1', NULL, 'page', NULL, 11, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-20', '2020-03-20'),
(5, 'Contact', 'contact', NULL, '', '', NULL, '1', NULL, 'page', NULL, 11, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2030-11-01 00:00:00', '2030-11-01 00:00:00', '2020-03-09', '2020-03-09'),
(6, 'Save more with GO! We give you the lowest prices on all your grocery needs.', 'save-more-with-go-we-give-you-the-lowest-prices-on-all-your-grocery-needs', 'about.jpg', '<h5><span style=\"font-size:16px\">Our Vision</span></h5>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here,</p>\r\n\r\n<h5><span style=\"font-size:16px\">Our Goal</span></h5>\r\n\r\n<p>When looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, Lorem Ipsum has been the industry&#39;s standard dummy text ever since.</p>\r\n', NULL, NULL, '1', '', 'post', NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-03-09', '2020-03-09');

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

DROP TABLE IF EXISTS `post_categories`;
CREATE TABLE IF NOT EXISTS `post_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `post_id` int(5) NOT NULL,
  `position` varchar(60) DEFAULT NULL,
  `categorie_id` int(5) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_categories`
--

INSERT INTO `post_categories` (`id`, `post_id`, `position`, `categorie_id`, `created_at`, `updated_at`) VALUES
(12, 2, 'position_2', 7, NULL, NULL),
(16, 5, 'position_1', 8, NULL, NULL),
(15, 5, 'position_2', 9, NULL, NULL),
(72, 3, 'position_1', 26, NULL, NULL),
(65, 1, 'position_2', 24, NULL, NULL),
(64, 1, 'position_1', 18, NULL, NULL),
(71, 3, 'position_2', 27, NULL, NULL),
(74, 4, 'position_1', 28, NULL, NULL),
(75, 4, 'position_1', 29, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_galleries`
--

DROP TABLE IF EXISTS `post_galleries`;
CREATE TABLE IF NOT EXISTS `post_galleries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `post_id` int(5) DEFAULT NULL,
  `gallerie_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_image`
--

DROP TABLE IF EXISTS `post_image`;
CREATE TABLE IF NOT EXISTS `post_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_slides`
--

DROP TABLE IF EXISTS `post_slides`;
CREATE TABLE IF NOT EXISTS `post_slides` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `slide_id` int(5) DEFAULT NULL,
  `post_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(250) DEFAULT NULL,
  `description` text,
  `status` int(10) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Administrator', NULL, 1, NULL, NULL),
(2, 'user', 'User', 'user', NULL, '2017-05-19', '2017-05-19'),
(3, 'admin', 'Admin', 'admin', NULL, '2017-05-19', '2017-05-19');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `website_name` varchar(250) NOT NULL,
  `website_url` varchar(250) DEFAULT NULL,
  `language` varchar(220) DEFAULT NULL,
  `address` text,
  `phone` text,
  `email` text,
  `work_time` text,
  `logo_image` varchar(250) DEFAULT NULL,
  `logo_text` varchar(250) DEFAULT NULL,
  `favicon_image` varchar(250) DEFAULT NULL,
  `copyright` varchar(200) DEFAULT NULL,
  `address_site` text,
  `is_slide_only_page` int(11) DEFAULT NULL,
  `link_fb` text,
  `user_id` int(5) DEFAULT NULL,
  `google_map` text,
  `facebooklink` text,
  `visitor_counter` text,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `website_name`, `website_url`, `language`, `address`, `phone`, `email`, `work_time`, `logo_image`, `logo_text`, `favicon_image`, `copyright`, `address_site`, `is_slide_only_page`, `link_fb`, `user_id`, `google_map`, `facebooklink`, `visitor_counter`, `created_at`, `updated_at`) VALUES
(1, 'Osahan Grocery', 'www.askbootstrap.com', '1', '<p><span style=\"background-color:rgb(249, 249, 249); color:rgb(136, 136, 136); font-family:maven pro,sans-serif\">86 Petersham town, New South wales Waedll Steet, Australia PA 6550</span></p>\r\n', '+91 12345-67890, (+91) 123 456 7890', 'info@gmail.com', '', 'logo.png', '', 'favicon.png', '<p><span style=\"background-color:rgb(249, 249, 249)\">TITB</span></p>\r\n', '<p><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d250151.1512034605!2d104.75009803674602!3d11.579666939586584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109513dc76a6be3%3A0x9c010ee85ab525bb!2sPhnom%20Penh!5e0!3m2!1sen!2skh!4v1582858467463!5m2!1sen!2skh\" style=\"border:0;\" width=\"688\"></iframe></p>\r\n', NULL, NULL, 11, NULL, NULL, NULL, '2020-02-28', '2020-02-28');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `language` int(220) DEFAULT NULL,
  `description` text,
  `status` int(10) DEFAULT NULL,
  `slide_type_id` int(5) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `deleted` int(10) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `name`, `language`, `description`, `status`, `slide_type_id`, `user_id`, `deleted`, `publish_date`, `unpublish_date`, `created_at`, `updated_at`) VALUES
(1, 'slide home', 1, '', 1, 1, 11, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2020-02-28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slide_image`
--

DROP TABLE IF EXISTS `slide_image`;
CREATE TABLE IF NOT EXISTS `slide_image` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `slide_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slide_image`
--

INSERT INTO `slide_image` (`id`, `image`, `link`, `slide_id`) VALUES
(1, 'slider1.jpg', NULL, 1),
(2, 'slider2.jpg', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slide_type`
--

DROP TABLE IF EXISTS `slide_type`;
CREATE TABLE IF NOT EXISTS `slide_type` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `description` text,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slide_type`
--

INSERT INTO `slide_type` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'slide home', '                ', '2020-02-28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

DROP TABLE IF EXISTS `socials`;
CREATE TABLE IF NOT EXISTS `socials` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `class` varchar(200) DEFAULT NULL,
  `image_icon` varchar(250) DEFAULT NULL,
  `link` varchar(250) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `user_id` int(5) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `name`, `class`, `image_icon`, `link`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'facebook', 'mdi mdi-facebook', NULL, 'https://www.facebook.com/', 1, 11, '2020-02-28', '2020-02-28'),
(2, 'twitter', 'mdi mdi-twitter', '', 'https://twitter.com/', 1, 11, '2020-02-28', NULL),
(3, 'instagram', 'mdi mdi-instagram', '', 'https://www.instagram.com/', 1, 11, '2020-02-28', NULL),
(4, 'What sapp', 'mdi mdi-whatsapp', '', 'https://www.whatsapp.com/', 1, 11, '2020-02-28', NULL),
(5, 'Messenger', 'mdi mdi-facebook-messenger', '', 'https://www.messenger.com/', 1, 11, '2020-02-28', NULL),
(6, 'google', 'mdi mdi-google', '', 'https://www.google.com/', 1, 11, '2020-02-28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `address` text,
  `status` int(10) DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `phone`, `image`, `branch_id`, `address`, `status`, `password`, `created_at`, `updated_at`, `remember_token`) VALUES
(11, 'cfa', 'admincfa', 'admincfa@gmail.com', '089 797 879', '14.jpg', 2, NULL, 1, '$2y$10$cHtTbmLV2xRINZH4kREUmewjUaXs4aKV8iWzqvIverwWJ71.V62WS', NULL, '2019-12-05', 'OVscTert764w6sCI71jzhqCiBPe8xGUeXebopSONpcHaHzkJur0mkbhWAvW2'),
(14, 'CFA', 'havchantha.79@gmail.com ', 'havchantha.79@gmail.com ', '015 56 45 79 ', '', 2, NULL, 1, '$2y$10$ws7Tm5gltbmj9V76SdP5Lu.GX8wNF0QOrsGGmoDYG39AE7oFnKuOm', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `role_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_id`, `user_id`) VALUES
(41, 3, 11),
(40, 2, 11),
(39, 1, 11),
(45, 1, 14),
(46, 2, 14),
(47, 3, 14);

-- --------------------------------------------------------

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
CREATE TABLE IF NOT EXISTS `widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `limit_show` int(11) DEFAULT NULL,
  `position` varchar(60) DEFAULT NULL,
  `page_side` varchar(60) DEFAULT NULL,
  `title` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `widget`
--

INSERT INTO `widget` (`id`, `category_id`, `post_id`, `language`, `limit_show`, `position`, `page_side`, `title`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 1, NULL, 'footer_1', 'logo_foot', 'Logo_foot', NULL, NULL),
(2, NULL, NULL, 1, NULL, 'footer_2', 'city_foot', 'City_foot', NULL, NULL),
(3, NULL, NULL, 1, NULL, 'footer_3', 'categories_foot', 'Categories_foot', NULL, NULL),
(4, NULL, NULL, 1, NULL, 'footer_4', 'about_foot', 'About_foot', NULL, NULL),
(5, NULL, NULL, 1, NULL, 'footer_5', 'download_foot', 'Download_foot', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
